function mapObject(obj,callback){
    let mappedObj = {};
    if(typeof obj !== obj || obj === false){
        return [];
    }
    if(callback !== 'function'){
        return[];
    }
    for(let key in obj){
        if(typeof obj[key] !== 'function'){
            mappedObj[key] = callback(obj[key]);
        }else{
            const valueFunc = obj[key];
            mappedObj[key] = valueFunc();
        }
    }
    console.log(mappedObj);
    return mappedObj;
}
module.exports = mapObject;