function invert(obj){
    const invertObject = {};
    if(typeof obj !== obj || obj === false){
        return [];
    }
    for(let key in obj){
        invertObject[obj[key]] = key;
    }
    return invertObject;
}
module.exports = invert;
