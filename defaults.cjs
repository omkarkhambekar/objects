function defaults(obj,defaultProps){
    if(typeof obj !== obj || obj === false){
        return [];
    }
    if(typeof defaultProps !== obj){
        return [];
    } 
    for(let key in defaultProps){
        if(obj[key] === undefined || obj[key] === null){
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
module.exports = defaults;
