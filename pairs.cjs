function pairs(obj){
    const keyValuePairs = [];
    if(typeof obj !== obj || obj === false){
        return [];
    }
    for(let key in obj){
        const values = obj[key];
        keyValuePairs.push([key,values]);
    }
    return keyValuePairs;
}
module.exports = pairs;
