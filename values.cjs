function values(obj){
    const value = [];
    if(typeof obj === false){
        return [];
    }
    for(let key in obj){
        if(typeof obj[key] !== 'function'){
            value.push(obj[key]);
        }
    }
    return value;
}
module.exports = values;