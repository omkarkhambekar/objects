function keys(obj){
    const allKeys = [];
    if(typeof obj !== 'object' || obj === false){
        return [];
    }
    for(let key in obj){
        
        allKeys.push(key);
    }
    return allKeys;
}
module.exports = keys;